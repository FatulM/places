// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:places/app/places_app.dart';

void main() {
  EquatableConfig.stringify = kDebugMode;

  runApp(PlacesApp());
}
