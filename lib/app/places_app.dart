// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:places/widgets/map/map_page.dart';

@immutable
class PlacesApp extends StatelessWidget {
  const PlacesApp({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Places',
      theme: ThemeData.light(),
      home: MapPage(),
    );
  }
}
