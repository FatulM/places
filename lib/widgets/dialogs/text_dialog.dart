// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TextDialog extends StatelessWidget {
  final String text;

  TextDialog({
    @required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Places'),
      content: Text(text),
      actions: [
        FlatButton(
          onPressed: Get.back,
          child: Text('OK'),
        ),
      ],
    );
  }
}
