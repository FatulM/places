// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:places/widgets/map/bloc/map_bloc.dart';
import 'package:places/widgets/map/bloc/map_states.dart';
import 'package:places/widgets/map/day_night_button.dart';
import 'package:places/widgets/map/location_button.dart';
import 'package:places/widgets/map/map_view.dart';
import 'package:places/widgets/map/places_list.dart';
import 'package:places/widgets/map/profile_button.dart';
import 'package:places/widgets/map/search_button.dart';

@immutable
class MapBody extends StatelessWidget {
  const MapBody({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final bloc = MapBloc.of(context);

    return Stack(
      fit: StackFit.expand,
      children: [
        MapView(
          mapLoadedCallback: bloc.mapLoaded,
          styleLoadedCallback: bloc.styleLoaded,
        ),
        SafeArea(
          child: BlocBuilder(
            cubit: bloc,
            builder: (BuildContext context, MapState state) {
              return Column(
                children: [
                  SizedBox(height: 16.0),
                  Row(
                    children: [
                      SizedBox(width: 16),
                      DayNightButton(
                        onPressed: bloc.toggleDayNight,
                      ),
                      Spacer(),
                      SearchButton(
                        onPressed: bloc.searchHere,
                      ),
                      Spacer(),
                      ProfileButton(
                        onPressed: () {},
                      ),
                      SizedBox(width: 16),
                    ],
                  ),
                  Spacer(),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(width: 16.0),
                      if (state.inProgress)
                        Padding(
                          padding: EdgeInsets.all(4.0),
                          child: CircularProgressIndicator(),
                        ),
                      Spacer(),
                      LocationButton(
                        onPressed: bloc.myLocation,
                      ),
                      SizedBox(width: 16.0),
                    ],
                  ),
                  SizedBox(height: 16.0),
                  if (state.venues != null)
                    Padding(
                      padding: EdgeInsets.only(
                        bottom: 8.0,
                      ),
                      child: PlacesList(
                        venues: state.venues,
                        onItemClick: bloc.itemClick,
                      ),
                    ),
                ],
              );
            },
          ),
        ),
      ],
    );
  }
}
