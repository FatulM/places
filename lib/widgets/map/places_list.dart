// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:places/widgets/map/places_list_item.dart';
import 'package:places/widgets/ui_models.dart';

@immutable
class PlacesList extends StatelessWidget {
  const PlacesList({
    Key key,
    @required this.venues,
    @required this.onItemClick,
  }) : super(key: key);

  final List<UiVenue> venues;
  final void Function(int index) onItemClick;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120.0,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: venues.length,
        itemBuilder: (BuildContext context, int index) {
          final item = venues[index];

          return Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 8.0,
            ),
            child: PlacesListItem(
              venue: item,
              onPressed: () {
                onItemClick(index);
              },
            ),
          );
        },
      ),
    );
  }
}
