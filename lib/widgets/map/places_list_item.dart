// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:places/widgets/ui_models.dart';

@immutable
class PlacesListItem extends StatelessWidget {
  const PlacesListItem({
    Key key,
    @required this.venue,
    @required this.onPressed,
  }) : super(key: key);

  final UiVenue venue;
  final void Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: AspectRatio(
        aspectRatio: 1.0,
        child: Material(
          elevation: 4.0,
          shape: RoundedRectangleBorder(
            side: venue.selected
                ? BorderSide(
              color: Theme.of(context).accentColor,
              width: 2.0,
              style: BorderStyle.solid,
            )
                : BorderSide.none,
            borderRadius: BorderRadius.circular(8.0),
          ),
          type: MaterialType.card,
          child: Column(
            children: [
              SizedBox(
                height: 8.0,
              ),
              Center(
                child: Image.network(
                  venue.iconUrl,
                  height: 48.0,
                  width: 48.0,
                  color: Theme.of(context).textTheme.subtitle1.color,
                  fit: BoxFit.fill,
                  errorBuilder: (_, __, ___) {
                    return Icon(
                      Icons.error_outline,
                      size: 48.0,
                    );
                  },
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      venue.name,
                      style: Theme.of(context).textTheme.subtitle1,
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
