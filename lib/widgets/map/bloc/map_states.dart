import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:places/widgets/ui_models.dart';

@immutable
class MapState extends Equatable {
  final List<UiVenue> venues;
  final bool inProgress;

  const MapState({
    @required this.venues,
    @required this.inProgress,
  }) : assert(inProgress != null);

  @override
  List<Object> get props => [venues, inProgress];

  factory MapState.empty() => MapState(
        venues: null,
        inProgress: false,
      );

  factory MapState.emptyLoading() => MapState(
        venues: null,
        inProgress: true,
      );

  factory MapState.data(List<UiVenue> venues) => MapState(
        venues: venues,
        inProgress: false,
      );

  factory MapState.dataLoading(List<UiVenue> venues) => MapState(
        venues: venues,
        inProgress: true,
      );
}
