// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:places/data/remote/remote.dart';
import 'package:places/widgets/details/details_dialog.dart';
import 'package:places/widgets/dialogs/text_dialog.dart';
import 'package:places/widgets/map/bloc/map_events.dart';
import 'package:places/widgets/map/bloc/map_states.dart';
import 'package:places/widgets/ui_models.dart';

class MapBloc extends Bloc<MapEvent, MapState> {
  static MapBloc of(BuildContext context) => BlocProvider.of<MapBloc>(context);

  final Remote remote;

  MapboxMapController _controller;
  List<UiVenue> _venues;

  MapBloc({@required this.remote}) : super(MapState.empty());

  @override
  Stream<MapState> mapEventToState(MapEvent event) async* {
    if (event is MapEventMapLoaded) {
      _setUpController(event.controller);
    } else if (event is MapEventStyleLoaded) {
      await _styleLoaded();
    } else if (event is MapEventMyLocation) {
      yield* _myLocation();
    } else if (event is MapEventSearchHere) {
      yield* _searchHere();
    } else if (event is MapEventItemClick) {
      yield* _itemClick(event.index);
    } else if (event is MapEventSymbolClick) {
      yield* _details(event.id);
    } else if (event is MapEventToggleDayNight) {
      _toggleDayNight();
    } else {
      throw 'unsupported';
    }
  }

  void _setUpController(MapboxMapController controller) {
    _controller = controller;
    if (_controller.onSymbolTapped.isEmpty) {
      _controller.onSymbolTapped.add((symbol) {
        symbolClick(symbol.data['venueId']);
      });
    }
  }

  Future<void> _styleLoaded() async {
    if (_venues != null) {
      await _clearSymbols();
      await _addSymbols();
    }
  }

  Stream<MapState> _myLocation() async* {
    _venues = null;
    yield MapState.emptyLoading();
    await _clearSymbols();
    yield* _getMyLocation();
    yield MapState.empty();
  }

  Stream<MapState> _searchHere() async* {
    _venues = null;
    yield MapState.emptyLoading();
    await _clearSymbols();
    try {
      var position = _controller.cameraPosition;
      final location = position.target;
      final zoom = position.zoom;

      final a = cos(51.500914631441454 * pi / 180) / (15.0 * 15.0);
      final b = cos(location.latitude * pi / 180) / (zoom * zoom);
      final dist = 350 * b / a;

      final List<VenueSearch> result = await remote.venueSearch(
        location.latitude,
        location.longitude,
        dist > 10000 ? 10000 : (dist < 100 ? 100 : dist),
      );

      if (result.isEmpty) {
        yield MapState.empty();
        return;
      }

      _venues = result.map((e) {
        final icon = e.categories.first.icon;

        return UiVenue(
          id: e.id,
          latitude: e.location.lat,
          longitude: e.location.lng,
          name: e.name,
          iconUrl: icon.prefix + "64" + icon.suffix,
          selected: false,
        );
      }).toList();

      await _addSymbols();
      yield MapState.data(_venues);
    } catch (_) {
      _venues = null;
      yield MapState.empty();
      _showTextDialog('No internet connection.');
    }
  }

  Stream<MapState> _itemClick(int index) async* {
    final item = _venues[index];
    final newSelected = !item.selected;

    _venues = _venues.map((e) {
      final same = e.id == item.id;
      return UiVenue(
        id: e.id,
        latitude: e.latitude,
        longitude: e.longitude,
        name: e.name,
        iconUrl: e.iconUrl,
        selected: same ? newSelected : false,
      );
    }).toList();

    await _clearSymbols();
    await _addSymbols();

    yield MapState.data(_venues);
  }

  Stream<MapState> _details(String id) async* {
    yield MapState.dataLoading(_venues);
    try {
      final venue = await remote.venueDetails(id);
      final category = venue.categories.firstWhere(
        (e) => e.primary,
        orElse: () => venue.categories.first,
      );
      final details = UiDetails(
        id: venue.id,
        name: venue.name,
        iconUrl: "${category.icon.prefix}64${category.icon.suffix}",
        url: venue.canonicalUrl ??
            "https://www.google.com/search?q=${venue.name}",
        phone: venue.contact?.formattedPhone ?? "Unknown Phone",
        address:
            venue.location?.formattedAddress?.reduce((a, b) => a + '\n' + b) ??
                "Unknown Address",
      );
      yield MapState.data(_venues);
      _showDetailsDialog(details);
    } catch (_) {
      yield MapState.data(_venues);
      _showTextDialog('No internet connection.');
    }
  }

  Future<void> _clearSymbols() async {
    await _controller.clearSymbols();
  }

  Future<void> _addSymbols() async {
    final theme = Get.theme;
    final ac = theme.accentColor;
    final tc = theme.textTheme.bodyText1.color;
    final acc = '#' + (0xffffff & ac.value).toRadixString(16).padLeft(6, '0');
    final tcc = '#' + (0xffffff & tc.value).toRadixString(16).padLeft(6, '0');

    await _controller.addSymbols(
      _venues.map((e) {
        return SymbolOptions(
          iconSize: 1.5,
          draggable: false,
          iconOpacity: 1.0,
          textColor: e.selected ? acc : tcc,
          textField: e.name,
          textOpacity: 1.0,
          textMaxWidth: 8.0,
          textOffset: Offset(0.0, 2.5),
          textSize: 18.0,
          iconImage: 'monument-15',
          iconHaloColor: e.selected ? acc : tcc,
          textHaloColor: e.selected ? acc : tcc,
          iconColor: e.selected ? acc : tcc,
          geometry: LatLng(e.latitude, e.longitude),
        );
      }).toList(),
      _venues.map((e) => {'venueId': e.id}).toList(),
    );
  }

  Stream<MapState> _getMyLocation() async* {
    final manager = Location.instance;
    final status = await manager.hasPermission();
    if (status == PermissionStatus.denied) {
      final newStatus = await manager.requestPermission();
      if (newStatus == PermissionStatus.denied ||
          newStatus == PermissionStatus.deniedForever) {
        _showTextDialog(
            'We need location permission in order to show you on map.');
        return;
      }
    } else if (status == PermissionStatus.deniedForever) {
      _showTextDialog(
          'We need location permission in order to show you on map.');
      return;
    }
    if (!await manager.serviceEnabled()) {
      await manager.requestService();
      return;
    }
    final location = await manager.getLocation();
    await _updateMapLocation(location.latitude, location.longitude);
  }

  Future<void> _updateMapLocation(double latitude, double longitude) async {
    await _controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(latitude, longitude),
          zoom: 15.0,
        ),
      ),
    );
  }

  void _showDetailsDialog(UiDetails details) {
    // not await
    Get.dialog(DetailsDialog(details: details));
  }

  void _showTextDialog(String text) {
    // not await
    Get.dialog(TextDialog(text: text));
  }

  void _toggleDayNight() {
    final theme = Get.theme;
    final isLight = theme.brightness == Brightness.light;
    Get.changeTheme(isLight ? ThemeData.dark() : ThemeData.light());
  }

  void mapLoaded(MapboxMapController controller) {
    add(MapEvent.mapLoaded(controller));
  }

  void searchHere() {
    add(MapEvent.searchHere());
  }

  void myLocation() {
    add(MapEvent.myLocation());
  }

  void itemClick(int index) {
    add(MapEvent.itemClick(index));
  }

  void symbolClick(id) {
    add(MapEvent.symbolClick(id));
  }

  void toggleDayNight() {
    add(MapEvent.toggleDayNight());
  }

  void styleLoaded() {
    add(MapEvent.styleLoaded());
  }
}
