import 'package:equatable/equatable.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MapEvent extends Equatable {
  @override
  List<Object> get props => [];

  const MapEvent();

  factory MapEvent.mapLoaded(MapboxMapController controller) =>
      MapEventMapLoaded(controller: controller);

  factory MapEvent.myLocation() => MapEventMyLocation();

  factory MapEvent.searchHere() => MapEventSearchHere();

  factory MapEvent.symbolClick(String id) => MapEventSymbolClick(id: id);

  factory MapEvent.itemClick(int index) => MapEventItemClick(index: index);

  factory MapEvent.toggleDayNight() => MapEventToggleDayNight();

  factory MapEvent.styleLoaded() => MapEventStyleLoaded();
}

@immutable
class MapEventMapLoaded extends MapEvent {
  final MapboxMapController controller;

  const MapEventMapLoaded({@required this.controller});

  @override
  List<Object> get props => [controller];
}

@immutable
class MapEventMyLocation extends MapEvent {
  const MapEventMyLocation();
}

@immutable
class MapEventSearchHere extends MapEvent {
  const MapEventSearchHere();
}

@immutable
class MapEventSymbolClick extends MapEvent {
  final String id;

  const MapEventSymbolClick({@required this.id});

  @override
  List<Object> get props => [id];
}

@immutable
class MapEventItemClick extends MapEvent {
  final int index;

  const MapEventItemClick({@required this.index});

  @override
  List<Object> get props => [index];
}

@immutable
class MapEventToggleDayNight extends MapEvent {
  const MapEventToggleDayNight();
}

@immutable
class MapEventStyleLoaded extends MapEvent {
  const MapEventStyleLoaded();
}
