import 'package:flutter/material.dart';

class ProfileButton extends StatelessWidget {
  final void Function() onPressed;

  const ProfileButton({
    Key key,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Material(
        elevation: 4.0,
        type: MaterialType.circle,
        color: Theme.of(context).cardColor,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Icon(
            Icons.person,
            size: 32.0,
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
    );
  }
}
