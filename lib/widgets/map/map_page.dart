// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:places/data/remote/remote.dart';
import 'package:places/widgets/map/bloc/map_bloc.dart';
import 'package:places/widgets/map/map_body.dart';

@immutable
class MapPage extends StatelessWidget {
  const MapPage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => MapBloc(
          remote: Remote(),
        ),
        child: MapBody(),
      ),
    );
  }
}
