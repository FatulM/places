// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';

class SearchButton extends StatelessWidget {
  final void Function() onPressed;

  const SearchButton({
    Key key,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Material(
        elevation: 4.0,
        borderRadius: BorderRadius.circular(8.0),
        type: MaterialType.card,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 12.0,
          ),
          child: Text(
            'Search Here',
            style: Theme.of(context).textTheme.headline5,
          ),
        ),
      ),
      onTap: onPressed,
    );
  }
}
