// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:places/app/constants.dart';

@immutable
class MapView extends StatelessWidget {
  final void Function(MapboxMapController controller) mapLoadedCallback;
  final void Function() styleLoadedCallback;

  const MapView({
    Key key,
    @required this.mapLoadedCallback,
    @required this.styleLoadedCallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MapboxMap(
      accessToken: mapBoxAccessToken,
      styleString: Theme.of(context).brightness == Brightness.light
          ? MapboxStyles.LIGHT
          : MapboxStyles.DARK,
      initialCameraPosition: CameraPosition(
        target: LatLng(
          51.500914631441454,
          -0.12643894603064715,
        ),
        zoom: 15.0,
      ),
      tiltGesturesEnabled: false,
      rotateGesturesEnabled: false,
      scrollGesturesEnabled: true,
      zoomGesturesEnabled: true,
      onMapCreated: mapLoadedCallback,
      trackCameraPosition: true,
      onStyleLoadedCallback: styleLoadedCallback,
    );
  }
}
