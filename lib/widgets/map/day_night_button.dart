import 'package:flutter/material.dart';

class DayNightButton extends StatelessWidget {
  final void Function() onPressed;

  const DayNightButton({
    Key key,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Material(
        elevation: 4.0,
        type: MaterialType.circle,
        color: Theme.of(context).cardColor,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Icon(
            Theme.of(context).brightness == Brightness.light
                ? Icons.wb_sunny
                : Icons.nights_stay,
            size: 32.0,
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
    );
  }
}
