// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';

@immutable
class LocationButton extends StatelessWidget {
  final void Function() onPressed;

  const LocationButton({
    Key key,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Material(
        elevation: 4.0,
        type: MaterialType.circle,
        color: Theme.of(context).cardColor,
        child: Padding(
          padding: EdgeInsets.all(4.0),
          child: Icon(
            Icons.location_on,
            size: 48.0,
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
      onTap: onPressed,
    );
  }
}
