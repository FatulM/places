import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class UiVenue extends Equatable {
  final String id;
  final String name;
  final String iconUrl;
  final double latitude;
  final double longitude;
  final bool selected;

  const UiVenue({
    @required this.id,
    @required this.latitude,
    @required this.longitude,
    @required this.name,
    @required this.iconUrl,
    @required this.selected,
  })  : assert(id != null),
        assert(latitude != null),
        assert(longitude != null),
        assert(name != null),
        assert(iconUrl != null),
        assert(selected != null);

  @override
  List<Object> get props => [
        id,
        name,
        iconUrl,
        latitude,
        longitude,
        selected,
      ];
}

@immutable
class UiDetails extends Equatable {
  final String id;
  final String name;
  final String iconUrl;
  final String url;
  final String phone;
  final String address;

  const UiDetails({
    @required this.id,
    @required this.name,
    @required this.iconUrl,
    @required this.url,
    @required this.phone,
    @required this.address,
  })  : assert(id != null),
        assert(name != null),
        assert(iconUrl != null),
        assert(url != null),
        assert(phone != null),
        assert(address != null);

  @override
  List<Object> get props => [
        id,
        name,
        iconUrl,
        url,
        phone,
        address,
      ];
}
