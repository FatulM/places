// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:places/widgets/ui_models.dart';

@immutable
class DetailsBody extends StatelessWidget {
  final UiDetails details;
  final void Function() backClick;

  const DetailsBody({
    Key key,
    @required this.details,
    @required this.backClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 460.0,
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                GestureDetector(
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.arrow_back,
                      size: 24.0,
                    ),
                  ),
                  onTap: backClick,
                ),
                Spacer(),
              ],
            ),
            Image.network(
              details.iconUrl,
              height: 128.0,
              width: 128.0,
              color: Theme.of(context).textTheme.subtitle1.color,
              fit: BoxFit.fill,
              errorBuilder: (_, __, ___) => Icon(
                Icons.error_outline,
                size: 128.0,
              ),
            ),
            Spacer(),
            Text(
              details.name,
              style: Theme.of(context).textTheme.headline6,
              textAlign: TextAlign.center,
              softWrap: true,
            ),
            SizedBox(height: 16.0),
            Text(
              details.phone,
              style: Theme.of(context).textTheme.headline6,
              textAlign: TextAlign.center,
              softWrap: true,
            ),
            SizedBox(height: 16.0),
            Text(
              details.address,
              style: Theme.of(context).textTheme.headline6,
              textAlign: TextAlign.center,
              softWrap: true,
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
