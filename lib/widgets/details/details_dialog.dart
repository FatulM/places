// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:places/widgets/details/details_body.dart';
import 'package:places/widgets/ui_models.dart';

@immutable
class DetailsDialog extends StatelessWidget {
  final UiDetails details;

  const DetailsDialog({
    Key key,
    @required this.details,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 8.0,
      child: DetailsBody(
        details: details,
        backClick: () {
          Get.back();
        },
      ),
    );
  }
}
