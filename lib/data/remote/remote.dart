// Copyright 2018 - 2020, Amirreza Madani. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:places/app/constants.dart';

class VenueSearchResult {
  VenueSearchResult({
    @required this.response,
  });

  final VenueSearchResponse response;

  factory VenueSearchResult.fromMap(Map<String, dynamic> json) =>
      VenueSearchResult(
        response: VenueSearchResponse.fromMap(json["response"]),
      );
}

class VenueSearchResponse {
  VenueSearchResponse({
    @required this.venues,
  });

  final List<VenueSearch> venues;

  factory VenueSearchResponse.fromMap(Map<String, dynamic> json) =>
      VenueSearchResponse(
        venues: List<VenueSearch>.from(
          json["venues"].map((x) => VenueSearch.fromMap(x)),
        ),
      );
}

class VenueSearch {
  VenueSearch({
    @required this.id,
    @required this.name,
    @required this.location,
    @required this.categories,
  });

  final String id;
  final String name;
  final VenueSearchLocation location;
  final List<VenueSearchCategory> categories;

  factory VenueSearch.fromMap(Map<String, dynamic> json) => VenueSearch(
    id: json["id"],
        name: json["name"],
        location: VenueSearchLocation.fromMap(json["location"]),
        categories: List<VenueSearchCategory>.from(
          json["categories"].map((x) => VenueSearchCategory.fromMap(x)),
        ),
      );
}

class VenueSearchCategory {
  VenueSearchCategory({
    @required this.id,
    @required this.name,
    @required this.icon,
  });

  final String id;
  final String name;
  final VenueSearchIcon icon;

  factory VenueSearchCategory.fromMap(Map<String, dynamic> json) =>
      VenueSearchCategory(
        id: json["id"],
        name: json["name"],
        icon: VenueSearchIcon.fromMap(json["icon"]),
      );
}

class VenueSearchIcon {
  VenueSearchIcon({
    @required this.prefix,
    @required this.suffix,
  });

  final String prefix;
  final String suffix;

  factory VenueSearchIcon.fromMap(Map<String, dynamic> json) => VenueSearchIcon(
        prefix: json["prefix"],
        suffix: json["suffix"],
      );
}

class VenueSearchLocation {
  VenueSearchLocation({
    @required this.lat,
    @required this.lng,
  });

  final double lat;
  final double lng;

  factory VenueSearchLocation.fromMap(Map<String, dynamic> json) =>
      VenueSearchLocation(
        lat: json["lat"].toDouble(),
        lng: json["lng"].toDouble(),
      );
}

class VenueResult {
  VenueResult({
    @required this.response,
  });

  final VenueResponse response;

  factory VenueResult.fromMap(Map<String, dynamic> json) => VenueResult(
        response: VenueResponse.fromMap(json["response"]),
      );
}

class VenueResponse {
  VenueResponse({
    @required this.venue,
  });

  final Venue venue;

  factory VenueResponse.fromMap(Map<String, dynamic> json) => VenueResponse(
        venue: Venue.fromMap(json["venue"]),
      );
}

class Venue {
  Venue({
    @required this.id,
    @required this.name,
    @required this.contact,
    @required this.location,
    @required this.canonicalUrl,
    @required this.categories,
  });

  final String id;
  final String name;
  final VenueContact contact;
  final VenueLocation location;
  final String canonicalUrl;
  final List<VenueCategory> categories;

  factory Venue.fromMap(Map<String, dynamic> json) => Venue(
    id: json["id"],
        name: json["name"],
        contact: VenueContact.fromMap(json["contact"]),
        location: VenueLocation.fromMap(json["location"]),
        canonicalUrl: json["canonicalUrl"],
        categories: List<VenueCategory>.from(
          json["categories"].map((x) => VenueCategory.fromMap(x)),
        ),
      );
}

class VenueCategory {
  VenueCategory({
    @required this.id,
    @required this.name,
    @required this.icon,
    @required this.primary,
  });

  final String id;
  final String name;
  final VenueIcon icon;
  final bool primary;

  factory VenueCategory.fromMap(Map<String, dynamic> json) => VenueCategory(
        id: json["id"],
        name: json["name"],
        icon: VenueIcon.fromMap(json["icon"]),
        primary: json["primary"] ?? false,
      );
}

class VenueIcon {
  VenueIcon({
    @required this.prefix,
    @required this.suffix,
  });

  final String prefix;
  final String suffix;

  factory VenueIcon.fromMap(Map<String, dynamic> json) => VenueIcon(
        prefix: json["prefix"],
        suffix: json["suffix"],
      );
}

class VenueContact {
  VenueContact({
    @required this.formattedPhone,
  });

  final String formattedPhone;

  factory VenueContact.fromMap(Map<String, dynamic> json) => VenueContact(
        formattedPhone: json["formattedPhone"],
      );
}

class VenueLocation {
  VenueLocation({
    @required this.lat,
    @required this.lng,
    @required this.formattedAddress,
  });

  final double lat;
  final double lng;
  final List<String> formattedAddress;

  factory VenueLocation.fromMap(Map<String, dynamic> json) => VenueLocation(
    lat: json["lat"].toDouble(),
        lng: json["lng"].toDouble(),
        formattedAddress: List<String>.from(
          json["formattedAddress"].map((x) => x),
        ),
      );
}

class ApiException {
  @override
  String toString() => 'ApiException';
}

class Remote {
  Future<List<VenueSearch>> venueSearch(
    final double lat,
    final double lng,
    final double dist,
  ) async {
    try {
      final response = await http.get(
        '$foursquareBaseUrl/v2/venues/search'
        '?client_id=$forsquareClientId'
        '&client_secret=$forsquareClientSecret'
        '&v=20300101'
        '&limit=10'
        '&radius=$dist'
        '&ll=$lat,$lng',
      );

      if (response.statusCode != 200) throw 'statusCode != 200';

      final map = json.decode(response.body);
      final result = VenueSearchResult.fromMap(map);

      return result.response.venues;
    } catch (_) {
      throw ApiException();
    }
  }

  Future<Venue> venueDetails(final String venueId) async {
    try {
      final response = await http.get(
        '$foursquareBaseUrl/v2/venues/$venueId'
        '?client_id=$forsquareClientId'
        '&client_secret=$forsquareClientSecret'
        '&v=20300101',
      );

      if (response.statusCode != 200) throw 'statusCode != 200';

      final map = json.decode(response.body);
      final result = VenueResult.fromMap(map);

      return result.response.venue;
    } catch (_) {
      throw ApiException();
    }
  }
}
