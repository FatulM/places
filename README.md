<img src="images/logo.png" alt="Places"/>

# Places

An awesome Flutter project.

## App Screenshots

<img src="images/img01.png" alt="screenshot 01" width="50%" /><img src="images/img02.png" alt="screenshot 02" width="50%" />

<img src="images/img03.png" alt="screenshot 03" width="50%" /><img src="images/img04.png" alt="screenshot 04" width="50%" />

<img src="images/img05.png" alt="screenshot 05" width="50%" /><img src="images/img06.png" alt="screenshot 06" width="50%" />

<img src="images/img07.png" alt="screenshot 07" width="50%" /><img src="images/img08.png" alt="screenshot 08" width="50%" />

<img src="images/img09.png" alt="screenshot 09" width="50%" /><img src="images/img10.png" alt="screenshot 10" width="50%" />

<img src="images/img11.png" alt="screenshot 11" width="50%" /><img src="images/img12.png" alt="screenshot 12" width="50%" />
